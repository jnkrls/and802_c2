package seguridad.and802.sharedpreferences

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //Variable para poder manejar el sharedPreferences
    var editSP:SharedPreferences.Editor? = null //Este sirve para almacenar informacion
    var preferences: SharedPreferences? = null

    //Variable constante la cual va a tener el nombre del SharedPreferences
    companion object {
        const val PREFERENCES = "DATOS_PREFERENCE"
        const val KEY_NOMBRE = "nombre"
        const val KEY_EDAD = "edad"
        const val KEY_SEND = "send"


        const val KEY_NOMBRE_DOS = "nombre_dos"
        const val KEY_EDAD_DOS = "edad_dos"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()


        btnGuardar.setOnClickListener{

            val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.hideSoftInputFromWindow(etEdad.windowToken,0)

            //Creamos el sharedPreferences en el cual vamos a almacenar la informacion
            editSP= getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit()

            //Obtener los datos que vamos a almacenar
            val nombre = etNombre.text.toString()
            val edad = etEdad.text.toString()

            //Guardamos la informacion en el sharedpreference
            editSP!!.putString(KEY_NOMBRE, nombre)
            editSP!!.putString(KEY_EDAD, edad)

            editSP!!.apply()

            limpiarCampos()

            Toast.makeText(this, "Se guardo la informacion", Toast.LENGTH_SHORT).show()

        }

        btnBorrarDatos.setOnClickListener{
            editSP = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit()
            editSP!!.clear()

            editSP!!.apply()

            Toast.makeText(this, "Se borró la informacion", Toast.LENGTH_SHORT).show()

        }

        btnMostrar.setOnClickListener{
            val intent = Intent(this,MostrarActivity::class.java)
            startActivity(intent)
        }

        btnEnviar.setOnClickListener{
            val intent = Intent(this, MostrarActivity::class.java)

            val nombre = etNombre.text.toString()
            val edad = etEdad.text.toString()

            //Enviamos los datos a traves del intent
            intent.putExtra(KEY_NOMBRE, nombre)
            intent.putExtra(KEY_EDAD, edad)
            intent.putExtra(KEY_SEND, true)

            //Enviar a traves de un Bundle
            val bundle = Bundle()

            bundle.putString(KEY_NOMBRE_DOS, nombre)
            bundle.putString(KEY_EDAD_DOS, edad)

            intent.putExtras(bundle)

            startActivity(intent)
        }

    }

    fun limpiarCampos():Unit{
        etNombre.setText("")
        etEdad.setText("")

        etNombre.requestFocus()

    }

}
