package seguridad.and802.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_mostrar.*

class MostrarActivity : AppCompatActivity() {

    //Lectura
    var preferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar)

    }

    override fun onResume() {
        super.onResume()

        val send = intent.getBooleanExtra(MainActivity.KEY_SEND, false)

        if(send){
            //Siempre que se envie a través del intent
            getValuesFromIntent()


        }else{
            getValuesFromSharedPreferences()
        }


    }

    fun getValuesFromIntent():Unit{
        val nombre = intent.getStringExtra(MainActivity.KEY_NOMBRE)
        val edad = intent.getStringExtra(MainActivity.KEY_EDAD)


        val bundle = intent.extras

        var nombreDos: String? = null
        var edadDos: String? = null
        if(bundle != null){
            if(bundle.containsKey(MainActivity.KEY_NOMBRE_DOS))
                nombreDos = bundle.getString(MainActivity.KEY_NOMBRE_DOS)
            if(bundle.containsKey(MainActivity.KEY_EDAD_DOS))
                edadDos = bundle.getString(MainActivity.KEY_EDAD_DOS)
        }

        tvNombre.text = "Intent: $nombre | Bundle: $nombreDos"
        tvEdad.text = "Intent: $edad | Bundle: $edadDos"

    }

    fun getValuesFromSharedPreferences():Unit{
        //Declaramos el sharedPreferences para poder obtener la info
        preferences = getSharedPreferences(MainActivity.PREFERENCES, Context.MODE_PRIVATE)

        //Obtenemos los datos del sharedPreferences a partir de la KEY
        var nombre  = preferences!!.getString(MainActivity.KEY_NOMBRE,"")
        var edad    = preferences!!.getString(MainActivity.KEY_EDAD,"")

        tvNombre.text = nombre
        tvEdad.text = edad
    }
}
